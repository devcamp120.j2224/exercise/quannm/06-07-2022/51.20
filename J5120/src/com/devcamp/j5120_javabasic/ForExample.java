package com.devcamp.j5120_javabasic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;
import java.util.function.IntFunction;

public class ForExample {
    public static void main(String[] args) throws Exception {
        /*ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(25);
        arrayList.add(13);
        arrayList.add(35);
        arrayList.add(54);
        arrayList.add(56);
        arrayList.add(45);
        arrayList.add(58);
        arrayList.add(87);
        arrayList.add(65);
        arrayList.add(88);
        arrayList.add(37);
        arrayList.add(99);
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));
        }

        System.out.println("-----------------");
        Random random = new Random();
        Integer[] arrayList1 = new Integer[20];
        // Create list number
        for (int i = 0; i < 20; i++) {
            arrayList1[i] = Integer.valueOf(random.nextInt());
        }
        for (int i = 0; i < arrayList1.length; i++) {
            System.out.println(arrayList1[i]);
        }*/

        /*String[] strings = new String[] {"a", "b", "c", "d"};
        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(strings));
        System.out.println(arrayList);

        System.out.println("-----------------");
        Integer[] arr = {1, 2, 3, 4, 5, 6, 7};
        ArrayList<Integer> arrayList1 = new ArrayList<>();
        Collections.addAll(arrayList1, arr);
        System.out.println(arrayList1);

        System.out.println("-----------------");
        Integer[] arr1 = {1, 2, 3, 4, 5, 6, 7};
        ArrayList<Integer> arrayList2 = new ArrayList<>();
        for (Integer i : arr1) {
            arrayList2.add(i);
        }
        System.out.println(arrayList2);*/

        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        Integer[] results = arrayList.stream().toArray(size -> new Integer[size]);
        for (Integer i : results) {
            System.out.println(i + " ");
        }

        System.out.println("-----------------");
        ArrayList<Integer> arrayList1 = new ArrayList<>();
        arrayList1.add(1);
        arrayList1.add(2);
        arrayList1.add(3);
        Integer[] results1 = arrayList1.stream().toArray(Integer[]::new);
        for (Integer i : results1) {
            System.out.println(i + " ");
        }

        System.out.println("-----------------");
        ArrayList<Integer> arrayList2 = new ArrayList<>();
        arrayList2.add(1);
        arrayList2.add(2);
        arrayList2.add(3);
        int[] results2 = arrayList1.stream().mapToInt(i -> i).toArray();
        for (Integer i : results2) {
            System.out.println(i + " ");
        }
    } 

}
